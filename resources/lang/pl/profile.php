<?php

return [

	'profile' => 'Profil',
	'profile_text' => 'tutaj możesz wpisać swoje dane o swoim doświadczeniu',
	'status_q' => 'Gdzie jesteś w poszukiwaniu pracy?',
	'role_q' => 'Rodzaj roli, której szukasz?',
	'exp_q' => 'Ile masz lat doświadczenia?',
	'tech_q' => 'Oceń swoje najlepsze umiejętności i technologie?',
	'sallary_q' => 'Jaka jest Twoja oczekiwana miesięczna pensja? (B2B/Faktura - netto Stała - brutto.)',
	'town_q' => 'Wybierz miejsce, w którym chcesz pracować (Polska)',
	'info' => 'Informacja',
	'phone' => 'Numer telefonu',
	'about' => 'O mnie',
	'photo' => 'Twoje zdjęcie',
	'dropbox_photo' => 'Prześlij swoje zdjęcie tutaj!',
	'private_data' => 'Podstawowe dane',
	'education' => 'Edukacja',
	'choose_education' => 'Wybierz swoją uczelnię',
	'git_q' => 'Dodatkowe profile społecznościowe',
	'git_profile' => 'Twój profil na Github/Gitlab ',
	'cv_q' => 'Prześlij swój plik CV lub profil na LinkedIn',
	'dropbox_cv' => 'Prześlij swój plik CV tutaj!',
	'linkedin' => 'lub link do Twojej strony LinkedIn',
	'link_cv' => 'Link do twojego CV',
	'main_page' => 'Strona główna',
	'logout' => 'Wyloguj',
	'name' => 'Imię',
	'surname' => 'Nazwisko',
	'city' => 'Miasto',
	'save' => 'Zapisz'


];