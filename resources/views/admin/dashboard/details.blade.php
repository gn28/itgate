@extends('admin.dashboard.partials.layout')

@section('content')
 <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{ $user->name . ' ' . $user->surname ?? $user->surname }}</h4>
                        @if($user->photo)
                            <img src="{{'/' . $user->photo}}" width="200px">
                        @endif 
                    </div>

                    <div class="card-body">
                        <div class="h5">
                            Status: {{ $user->status->name_pl }} 
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="h5">
                            Email: {{ $user->email }}
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="h5">
                            Phone: {{ $user->phone }}
                        </div>
                    </div>
                    
                    <div class="card-body">
                        <div class="h5">
                            City (from): {{ $user->city }}
                        </div>
                    </div>

                    <div class="card-body">
                        <div class="h5">
                            Towns (to): 
                            @if($user->towns->count())
                                @foreach($$user->towns as $town)
                                    @if($loop->first)
                                        {{ $town->name }}
                                    @else
                                        {{ ', ' . $town->name }}
                                    @endif 
                                @endforeach
                            @endif
                        </div>
                    </div>

                    <div class="card-body">
                        <div class="h5">
                            Roles: 
                            @if($user->roles->count())
                                @foreach($$user->roles as $role)
                                    @if($loop->first)
                                        {{ $role->name }}
                                    @else
                                        {{ ', ' . $role->name }}
                                    @endif 
                                @endforeach
                            @endif
                        </div>
                    </div>

                    <div class="card-body">
                        <div class="h5">
                            About: <br><br> 
                            {{ $user->about }}
                        </div>
                    </div>

                    <div class="card-body">
                        <div class="h5">
                            Technologies: <br><br> 
                            {{ $user->technologies }}
                        </div>
                    </div>

                    <div class="card-body">
                        <div class="h5">
                            Education: {{ $user->education }} 
                        </div>
                    </div>

                    <div class="card-body">
                        <div class="h5">
                            Git profile: {{ $user->git_profile }} 
                        </div>
                    </div>

                    <div class="card-body">
                        <div class="h5">
                            Linkedin: {{ $user->linked }} 
                        </div>
                    </div>

                    <div class="card-body">
                        <div class="h5">
                            CV: <a href="{{ '/'. $user->cv }}" target="_blank">Link</a> 
                        </div>
                    </div>

                    <div class="card-body">
                        <div class="h5">
                            Sallary: {{ $user->sallary . ' ' .  $user->currency ?? $user->currency->name }} 
                        </div>
                    </div>
                    


                </div>
            </div>
       </div>
    </div>
@endsection
