@extends('admin.dashboard.partials.layout')

@section('content')
 <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Users</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-responsive-sm">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Email</th>
                                        <th>First name</th>
                                        <th>Second name</th>
                                        <th>City</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($users as $user)
                                        <tr>
                                            <th>{{ $user->id }}</th>
                                            <td>{{ $user->email }}</td>
                                            <td>{{ $user->name }}</td>
                                            <td>{{ $user->surname }}</td>
                                            <td>
                                               {{ $user->city }}
                                            </td>
                                            <td>
                                                <a href="{{ route('dashboard.user.details', $user->id) }}" type="button" class="btn btn-square btn-outline-success">See details</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {{ $users->links('admin.dashboard.partials.pagination') }}
                        </div>

                    </div>
                    
                </div>
            </div>
       </div>
    </div>
   
@endsection
