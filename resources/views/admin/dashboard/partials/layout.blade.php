<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Backend</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="/focus-2/images/favicon.png">
    <link rel="stylesheet" href="/focus-2/vendor/owl-carousel/css/owl.carousel.min.css">
    <link rel="stylesheet" href="/focus-2/vendor/owl-carousel/css/owl.theme.default.min.css">
    <link href="/focus-2/vendor/jqvmap/css/jqvmap.min.css" rel="stylesheet">
    <link href="/focus-2/css/style.css" rel="stylesheet">
    @yield('style')
</head>

<body>

    <div id="preloader">
        <div class="sk-three-bounce">
            <div class="sk-child sk-bounce1"></div>
            <div class="sk-child sk-bounce2"></div>
            <div class="sk-child sk-bounce3"></div>
        </div>
    </div>

    <div id="main-wrapper">

        <div class="nav-header">
            <a href="index.html" class="brand-logo">
        <img class="logo-abbr" src="/focus-2/images/logo.png" alt=""> 
                <img class="logo-compact" src="/focus-2/images/logo-text.png" alt="">
                <img class="brand-title" src="/focus-2/images/logo-text.png" alt="">
            </a>

            <div class="nav-control">
                <div class="hamburger">
                    <span class="line"></span><span class="line"></span><span class="line"></span>
                </div>
            </div>
        </div>

        <div class="header">
            <div class="header-content">
                <nav class="navbar navbar-expand">
                    <div class="collapse navbar-collapse justify-content-between">
                        <div class="header-left">
                             
                        </div>
                        <ul class="navbar-nav header-right">
            
                            <li class="nav-item dropdown header-profile">
                                <a class="nav-link" href="#" role="button" data-toggle="dropdown">
                                    <i class="mdi mdi-account"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a href="./app-profile.html" class="dropdown-item">
                                        <i class="icon-user"></i>
                                        <span class="ml-2">Main page</span>
                                    </a>
                                    <a href="#" class="dropdown-item" onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                                        <i class="icon-key"></i>
                                        <span class="ml-2">Logout</span>
                                    </a>
                                     <form  style="display: none" action="{{route('logout')}}" method="POST" id="logout-form">
                                                @csrf
                                            </form>
                                </div>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>

        <div class="quixnav">
            <div class="quixnav-scroll">
                <ul class="metismenu" id="menu">
                    <li>
                        <a class="has-arrow" href="javascript:void()" aria-expanded="false"><i
                                class="icon icon-single-04"></i><span class="nav-text">Dashboard</span></a>
                        <ul aria-expanded="false">
                              <li><a href="{{ route('dashboard.users.list') }}">Users</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>


        <div class="content-body">
            @yield('content')
        </div>


        <div class="footer">
            <div class="copyright">
                <p>Copyright 2022</p> 
            </div>
        </div>

    </div>

    <script src="/focus-2/vendor/global/global.min.js"></script>
    <script src="/focus-2/js/quixnav-init.js"></script>
    <script src="/focus-2/js/custom.min.js"></script>


    <!-- Vectormap -->
    <script src="/focus-2/vendor/raphael/raphael.min.js"></script>
    <script src="/focus-2/vendor/morris/morris.min.js"></script>


    <script src="/focus-2/vendor/circle-progress/circle-progress.min.js"></script>
    <script src="/focus-2/vendor/chart.js/Chart.bundle.min.js"></script>

    <script src="/focus-2/vendor/gaugeJS/dist/gauge.min.js"></script>

    <!--  flot-chart js -->
    <script src="/focus-2/vendor/flot/jquery.flot.js"></script>
    <script src="/focus-2/vendor/flot/jquery.flot.resize.js"></script>

    <!-- Owl Carousel -->
    <script src="/focus-2/vendor/owl-carousel/js/owl.carousel.min.js"></script>

    <!-- Counter Up -->
    <script src="/focus-2/vendor/jqvmap/js/jquery.vmap.min.js"></script>
    <script src="/focus-2/vendor/jqvmap/js/jquery.vmap.usa.js"></script>
    <script src="/focus-2/vendor/jquery.counterup/jquery.counterup.min.js"></script>


    <script src="/focus-2/js/dashboard/dashboard-1.js"></script>
    @yield('script')

</body>

</html> 