<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="en">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>ITgate Profile</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
    <meta name="description" content="This is an example dashboard created using build-in elements and components.">
    <meta name="msapplication-tap-highlight" content="no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
<link href="/admin/main.css" rel="stylesheet"></head>
<body>
    <div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
        <div class="app-header header-shadow">
            <div class="app-header__logo">
                <div class="logo-src">
                    
                </div>
                <div class="header__pane ml-auto">
                    <div>
                        <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
            <div class="app-header__mobile-menu">
                <div>
                    <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                        <span class="hamburger-box">
                            <span class="hamburger-inner"></span>
                        </span>
                    </button>
                </div>
            </div>
            <div class="app-header__menu">
                <span>
                    <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                        <span class="btn-icon-wrapper">
                            <i class="fa fa-ellipsis-v fa-w-6"></i>
                        </span>
                    </button>
                </span>
            </div>    
            <div class="app-header__content">
                <div class="app-header-right">
                    <div class="header-btn-lg pr-0">
                        <div class="widget-content p-0">
                            <div class="widget-content-wrapper">
                                <div class="widget-content-left">
                                    <div class="btn-group lang-dv">
                                        <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="p-0 btn">
                                            {{ strtoupper($lang) }}
                                        </a>
                                        <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu dropdown-menu-right">
                                            <a id="pl-lang" class="dropdown-item">PL</a>
                                            <a id="ru-lang" class="dropdown-item">RU</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="widget-content-left">
                                    <div class="btn-group">
                                        <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="p-0 btn">
                                            <img width="42" class="rounded-circle" src="admin/assets/images/avatars/1.png" alt="">
                                            <i class="fa fa-angle-down ml-2 opacity-8"></i>
                                        </a>
                                        <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu dropdown-menu-right">
                                            <a href="{{ route('page.main')}}" type="button" tabindex="0" class="dropdown-item">{{ __('profile.main_page') }}</a>
                                            <a href="#" onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();" type="button" tabindex="0" class="dropdown-item" id="logout-link">{{ __('profile.logout') }}</a>
                                            <form  style="display: none" action="{{route('logout')}}" method="POST" id="logout-form">
                                                @csrf
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>        
                </div>
            </div>
        </div>             
        <div class="app-main">
                <div class="app-sidebar sidebar-shadow">
                    <div class="app-header__logo">
                        <div class="logo-src"></div>
                        <div class="header__pane ml-auto">
                            <div>
                                <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                                    <span class="hamburger-box">
                                        <span class="hamburger-inner"></span>
                                    </span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="app-header__mobile-menu">
                        <div>
                            <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                                <span class="hamburger-box">
                                    <span class="hamburger-inner"></span>
                                </span>
                            </button>
                        </div>
                    </div>
                    <div class="app-header__menu">
                        <span>
                            <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                                <span class="btn-icon-wrapper">
                                    <i class="fa fa-ellipsis-v fa-w-6"></i>
                                </span>
                            </button>
                        </span>
                    </div>    <div class="scrollbar-sidebar">
                        <div class="app-sidebar__inner">
                            <ul class="vertical-nav-menu">
                                <li>
                                    <a href="index.html" class="mm-active">
                                        <i class="metismenu-icon pe-7s-id"></i>
                                        {{ __('profile.profile') }}
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>    <div class="app-main__outer">
                    <div class="app-main__inner">
                        <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading">
                                    <div class="page-title-icon">
                                        <i class="pe-7s-id icon-gradient bg-mean-fruit">
                                        </i>
                                    </div>
                                    <div>{{ __('profile.profile') }}
                                        <div class="page-title-subheading">{{ __('profile.profile_text') }}
                                        </div>
                                    </div>
                                </div>
                                <div class="page-title-actions">
                                  
                                </div>    
                            </div>
                        </div>            
                        <div class="row">
                            <div class="col-md-12">
                                <div class="main-card mb-3 card">
                                    <div class="card-body"><h5 class="card-title">{{ __('profile.status_q') }}</h5>
                                        <div class="row">
                                            @foreach($statuses as $status)
                                                <div class="col">
                                                    <div class="form-check">
                                                        <input type="radio" name="status" class="form-check-input" value="{{ $status->id }}"
                                                        @if ($status->id == $user->status_id)
                                                            checked
                                                        @endif
                                                        />
                                                        <label class="form-check-label" for="exampleRadios1">
                                                            {{ $status->{'name_' . $lang} }}
                                                        </label>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                        <div class="divider"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="main-card mb-3 card">
                                    <div class="card-body"><h5 class="card-title">{{ __('profile.role_q') }}</h5>
                                        <div class="row" id="role-checkboxes">
                                            @foreach($roles as $role)
                                            <div class="col-md-3">
                                                <div class="form-check" >
                                                   <input id="" name="role" type="checkbox" value="{{ $role->id }}" class="form-check-input"
                                                   @if(in_array($role->id, $roleArr))
                                                        checked
                                                   @endif
                                                   />
                                                        <label class="form-check-label" for="closeButton">
                                                            {{ $role->name }}
                                                        </label>
                                                </div>
                                            </div>
                                            @endforeach
                                        </div>
                                        <div class="divider"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="main-card mb-3 card">
                                    <div class="card-body"><h5 class="card-title">{{ __('profile.exp_q') }}</h5>
                                        <div class="row">
                                            @foreach($experience as $item)
                                            <div class="col">
                                                <div class="form-check">
                                                    <input type="radio" name="exp" class="form-check-input" value="{{ $item->id }}"
                                                    @if ($user->status_id == $item->id)
                                                            checked
                                                    @endif
                                                    />
                                                    <label class="form-check-label" for="exampleRadios1">
                                                        {{ $item->period }}
                                                    </label>
                                                </div>
                                            </div>
                                            @endforeach
                                        </div>
                                        <div class="divider"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="main-card mb-3 card">
                                    <div class="card-body"><h5 class="card-title">{{ __('profile.tech_q') }}</h5>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <textarea name="tech" class="form-control" rows="12">{{ $user->technologies }}</textarea>
                                            </div>
                                        </div>
                                        <div class="divider"></div>
                                    </div>
                                </div>
                            </div>



                            <div class="col-md-12">
                                <div class="main-card mb-3 card">
                                    <div class="card-body"><h5 class="card-title">{{ __('profile.sallary_q') }}</h5>
                                        <div class="row">
                                            <div class="col-md-4 mb-3">
                                                <input type="text" class="form-control" name="sallary" value="{{ $user->sallary }}" required>
                                            </div>
                                            <div>
                                                <select class="mb-2 form-control" name="currency">
                                                    @foreach($currencies as $currency)
                                                        <option value="{{ $currency->id }}" 
                                                            {{ $currency->id == $user->currency_id ? 'selected' : '' }}>
                                                            {{ $currency->name }}               
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="divider"></div>
                                    </div>
                                </div>
                            </div>


                            <div class="col-md-12">
                                <div class="main-card mb-3 card">
                                    <div class="card-body"><h5 class="card-title">{{ __('profile.town_q') }}</h5>
                                        <div class="row" id="town-checkboxes">
                                            @foreach($towns as $town)
                                                <div class="col-md-3">
                                                    <div class="form-check">
                                                       <input type="checkbox" value="{{ $town->id }}" class="form-check-input"
                                                        @if(in_array($town->id, $townArr))
                                                            checked
                                                        @endif
                                                       />
                                                            <label class="form-check-label" for="closeButton">
                                                                {{ $town->name }}
                                                            </label>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                        <div class="divider"></div>
                                    </div>
                                </div>
                            </div>


                            <div class="col-md-12">
                                <div class="main-card mb-3 card">
                                    <div class="card-body"><h5 class="card-title">{{ __('profile.info') }}</h5>
                                        
                                        <div class="row">
                                            <div class="col-md-2">
                                                {{ __('profile.phone') }}    
                                            </div>
                                            <div class="col-md-5 mb-3">
                                                <input name="phone" type="text" class="form-control" placeholder="" 
                                                value="{{ $user->phone }}">
                                            </div>
                                            
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2">
                                                {{ __('profile.about') }}   
                                            </div>
                
                                            <div class="col-md-5 mb-3">
                                                  <textarea name="about" class="form-control" rows="6">
                                                    {{ $user->about }}
                                                </textarea>
                                            </div>
                                            
                                        </div>
                                        
                                        <div class="divider"></div>
                                    </div>
                                </div>
                            </div>


                            <div class="col-md-12">
                                <div class="main-card mb-3 card">
                                    <div class="card-body"><h5 class="card-title">{{ __('profile.photo') }}</h5>
                                        <div class="row dropzone-dv">

                                            <div class="col-md-3">
                                                <form action="{{ route('profile.upload.photo') }}"
                                                      class="dropzone"
                                                      id="image-dropzone">
                                                          <div class="dz-message" data-dz-message><span>{{ __('profile.dropbox_photo') }}</span></div>
                                                </form>
                                            </div>
                                            @if($user->photo)
                                                <div class="col-md-2">
                                                    <img src={{ '/' .$user->photo }} height="150px" />
                                                </div>
                                            @endif
                                        </div>
                                        <div class="divider"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="main-card mb-3 card">
                                    <div class="card-body"><h5 class="card-title">{{ __('profile.main_data') }}</h5>
                                        <div class="row">
                                            <div class="col-md-1">
                                                {{ __('profile.name') }}  
                                            </div>
                                            <div class="col-md-4 mb-3">
                                                <input name="first-name" type="text" class="form-control" placeholder="" 
                                                value="{{ $user->name }}">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-1">
                                               {{ __('profile.surname') }}  
                                            </div>
                
                                            <div class="col-md-4 mb-3">
                                                <input name="second-name" type="text" class="form-control" placeholder="" 
                                                value="{{ $user->surname }}">
                                            </div>
                                            
                                        </div>
                                        <div class="row">
                                            <div class="col-md-1">
                                               {{ __('profile.city') }}
                                            </div>
                                            <div class="col-md-4 mb-3">
                                                <input name="town-from" type="text" class="form-control" placeholder="" 
                                                value="{{ $user->city }}">
                                            </div>
                                            
                                        </div>

                                        <div class="divider"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="main-card mb-3 card">
                                    <div class="card-body"><h5 class="card-title">{{ __('profile.education') }}</h5>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input name="education" id="education" type="text" class="form-control" placeholder="{{ __('profile.choose_education') }}" value="{{ $user->education }}">
                                            </div>
                                        </div>
                                        <div class="divider"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="main-card mb-3 card">
                                    <div class="card-body"><h5 class="card-title">{{ __('profile.git_q') }}</h5>
                                        <div class="row">

                                            <div class="col-md-3">
                                                    {{ __('profile.git_profile') }} 
                                            </div>
                                            <div class="col-md-4 mb-3">
                                                <input name="git" type="text" class="form-control" placeholder="" 
                                                value="{{ $user->git_profile }}">
                                            </div>

                                        </div>
                                        <div class="divider"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="main-card mb-3 card">
                                    <div class="card-body"><h5 class="card-title">{{ __('profile.cv_q') }}</h5>
                                        <div class="row dropzone-dv">
                                                <div class="col-md-3">
                                                    <form action="{{ route('profile.upload.cv') }}"
                                                          class="dropzone"
                                                          id="cv-dropzone">
                                                              <div class="dz-message" data-dz-message><span>{{ __('profile.dropbox_cv') }}</span>
                                                              </div>
                                                    </form>
                                                </div>
                                                @if($user->cv)  
                                                    <div class="col-md-3">
                                                        <a href="{{ '/' . $user->cv }}" target="_blank" class="cv-link">{{ __('profile.link_cv') }}</a>
                                                    </div>    
                                                @endif
                                        </div>

                                        <div class="row">
                                            <div class="col-md-3">
                                                    {{ __('profile.linkedin') }} 
                                            </div>
                                            <div class="col-md-4 mb-3">
                                                <input name="linked" type="text" class="form-control" id="validationTooltip01" placeholder="" value="{{ $user->linked }}">
                                            </div>
                                        </div>

                                        <div class="divider"></div>
                                    </div>
                                </div>
                            </div>

                        <div class="col-md-12">
                                <div class="main-card mb-3 card">
                                    <div class="card-body">
                                        <button class="mb-2 mr-2 btn btn-primary btn-block" id="save-page">{{ __('profile.save') }}
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>


                        </div>
                        
                        <div>
                            

                        </div>
                            
                        </div>
                    </div>
                    <div class="app-wrapper-footer">
                        <div class="app-footer">
                            <div class="app-footer__inner">
                               
                            </div>
                        </div>
                    </div>    
                </div>
        </div>
    </div>

<script type="text/javascript" src="/admin/assets/scripts/main.js"></script></body>
<script src="https://unpkg.com/dropzone@5/dist/min/dropzone.min.js"></script>
<link rel="stylesheet" href="https://unpkg.com/dropzone@5/dist/min/dropzone.min.css" type="text/css" />
<script
          src="https://code.jquery.com/jquery-3.6.0.min.js"
          integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
          crossorigin="anonymous"></script>
<script
              src="https://code.jquery.com/ui/1.13.0/jquery-ui.min.js"
              integrity="sha256-hlKLmzaRlE8SCJC1Kw8zoUbU8BxA+8kR3gseuKfMjxA="
              crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.13.0/themes/smoothness/jquery-ui.css" type="text/css" />
<script>
Dropzone.options.cvDropzone = {
    maxFiles: 1,
    acceptedFiles: ".pdf,.doc,.docx,.rtf"
};

Dropzone.options.imageDropzone = {
    createImageThumbnails: true,
    maxFiles: 1,
    acceptedFiles: ".jpeg,.jpg,.png,.gif"
};

$(function() {
    $( "#education" ).autocomplete({
        source: '/get/univercities'
    });
});

$(document).on('click', '#save-page', function() {
    let token = $('meta[name="csrf-token"]').attr('content');
    let status = $('input[name="status"]:checked').val();
    let exp = $('input[name="exp"]:checked').val();


    let roles = [];
    $('#role-checkboxes input:checked').each(function() {
        roles.push($(this).attr('value'));
    })
    let towns = [];
    $('#town-checkboxes input:checked').each(function() {
        towns.push($(this).attr('value'));
    })


    let tech = $('textarea[name="tech"]').val();
    let sallary = $('input[name="sallary"]').val();
    let currency = $('select[name="currency"] option:selected').val();
    let phone = $('input[name="phone"]').val();
    let about = $('textarea[name="about"]').val();

    let firstName = $('input[name="first-name"]').val();
    let secondName = $('input[name="second-name"]').val();
    let townFrom = $('input[name="town-from"]').val();

    let education = $('input[name="education"]').val();
    let git = $('input[name="git"]').val();
    let linked = $('input[name="linked"]').val();

    $.ajax({
        type:'POST',
        url: `/profile/user/save`,
        data: {
            status_id: status,
            experience_id: exp,
            roles: roles,
            towns: towns,
            technologies: tech,
            sallary: sallary,
            currency_id: currency,
            phone: phone,
            about: about,
            name: firstName,
            surname: secondName,
            city: townFrom,
            education: education,
            git_profile: git,
            linked: linked,
            _token: token
        },
        dataType: 'json',
        success: function (data, status, xhr) {
            console.log(data);
        },
        error: function (jqXhr, textStatus, errorMessage) {
            console.log(errorMessage);
        }
    });
});

$(document).on('click', '#pl-lang', function() {
    window.location.href = '/profile/user';
});

$(document).on('click', '#ru-lang', function() {
    window.location.href = '/profile/user/ru';
});

</script>
<style>
    .dropzone-dv {
        padding: 20px 20px;
    }
    .lang-dv {
        margin-right: 10px;
    }
    .link-cv {
        font-size:  18px;
        color: black;
    }
</style>
</html>