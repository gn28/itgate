<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>IT Gate</title>

    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css" >
{{--    <link rel="stylesheet" type="text/css" href="assets/fonts/line-icons.css">--}}
      <link href="https://cdn.lineicons.com/3.0/lineicons.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="assets/css/slicknav.css">
    <link rel="stylesheet" type="text/css" href="assets/css/owl.carousel.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/owl.theme.css">
    <link rel="stylesheet" type="text/css" href="assets/css/slick.css" >
    <link rel="stylesheet" type="text/css" href="assets/css/slick-theme.css" >
    <link rel="stylesheet" type="text/css" href="assets/css/animate.css">
    <link rel="stylesheet" type="text/css" href="assets/css/main.css">
    <link rel="stylesheet" type="text/css" href="assets/css/responsive.css">
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <link rel="icon" type="image/x-icon" href="/favicon.ico">
  </head>
  <body>

    <header id="header-wrap">
      <nav class="navbar navbar-expand-lg fixed-top scrolling-navbar indigo">
        <div class="container">
          <div class="navbar-header">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-navbar" aria-controls="main-navbar" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
              <span class="icon-menu"></span>
              <span class="icon-menu"></span>
              <span class="icon-menu"></span>
            </button>
            <a  class="navbar-brand"><img src="assets/img/itgate_logo2.png" alt="" width="150px"></a>
          </div>
          <div class="collapse navbar-collapse" id="main-navbar">
            <ul class="navbar-nav mr-auto w-100 justify-content-left clearfix">
              <li class="nav-item active">
                <a class="nav-link" href="#hero-area">
                  {{ __('main.menu_user') }}
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#services">
                  {{ __('main.menu_employer') }}
                </a>
              </li>

            </ul>
              <select class="form-control lang-sel" style="width: 74px">
                  <option class="pl-lang">
                    PL
                  </option>
                  <option class="ru-lang"
                      @if($locale == 'ru')
                          selected
                      @endif
                  >
                    RU
                  </option>
                  <option class="ukr-lang"
                      @if($locale == 'ukr')
                          selected
                      @endif
                  >
                    UKR
                  </option>
              </select>
            <div class="btn-sing float-right">
                <a class="btn btn-border login-link" href="{{ route('login') }}">{{__('main.login')}}</a>
            </div>
              <div class="btn-sing float-right">
                  <a class="btn btn-border login-link" href="{{ route('register') }}">{{__('main.register')}}</a>
              </div>

        </div>

        <ul class="mobile-menu navbar-nav">
          <li>
            <a class="page-scroll" href="#hero-area">
              {{ __('main.menu_user') }}
            </a>
          </li>
          <li>
            <a class="page-scroll" href="#services">
              {{ __('main.menu_employer') }}
            </a>
          </li>
        </ul>
        </div>
      </nav>

      <div id="hero-area" class="hero-area-bg">
        <div class="overlay"></div>
        <div class="container">
          <div class="row">
            <div class="col-md-12 col-sm-12">
              <div class="contents text-center">
                <h2 class="head-title wow fadeInUp">ITgate</h2>
                 <div class="" data-wow-delay="0.3s" style="color:  #585b60; font-size: 16px; font-weight: bold;">
                  	{{ __('main.top_text') }}
                  </div>
              </div>
              <div class="img-thumb text-center wow fadeInUp" data-wow-delay="0.6s">
                <img class="img-fluid" src="assets/img/hero-3.jpg" alt="">
              </div>
            </div>
          </div>
        </div>
      </div>

    </header>

    <section id="services" class="section-padding">
      <div class="container">
        <div class="section-header text-center">

        </div>
        <div class="row">

          <div class="col-md-6 col-lg-4 col-xs-12">
            <div class="services-item wow fadeInRight" data-wow-delay="0.3s">
              <div class="icon">
                <i class="lni lni-question-circle"></i>
              </div>
              <div class="services-content">
                <h3><a class="bold">{{ __('main.q_1') }}</a></h3>
                <p>{{ __('main.a_1') }}</p>
              </div>
            </div>
          </div>

          <div class="col-md-6 col-lg-4 col-xs-12">
            <div class="services-item wow fadeInRight" data-wow-delay="0.6s">
              <div class="icon">
                <i class="lni lni-cog"></i>
              </div>
              <div class="services-content">
                <h3><a class="bold">{{ __('main.q_2') }}</a></h3>
                <p>{{ __('main.a_2') }}</p>
              </div>
            </div>
          </div>

          <div class="col-md-6 col-lg-4 col-xs-12">
            <div class="services-item wow fadeInRight" data-wow-delay="0.9s">
              <div class="icon">
                <i class="lni lni-user"></i>
              </div>
              <div class="services-content">
                <h3><a class="bold">{{ __('main.q_3') }}</a></h3>
                <p>{{ __('main.a_3') }}</p>
              </div>
            </div>
          </div>

          <div class="col-md-6 col-lg-4 col-xs-12">
            <div class="services-item wow fadeInRight" data-wow-delay="1.2s">
              <div class="icon">
                <i class="lni lni-world"></i>
              </div>
              <div class="services-content">
                <h3><a class="bold">{{ __('main.q_4') }}</a></h3>
                <p>{{ __('main.a_4') }}</p>
              </div>
            </div>
          </div>

          <div class="col-md-6 col-lg-4 col-xs-12">
            <div class="services-item wow fadeInRight" data-wow-delay="1.5s">
              <div class="icon">
                <i class="lni lni-wallet"></i>
              </div>
              <div class="services-content">
                <h3><a class="bold">{{ __('main.q_5') }}</a></h3>
                <p>{{ __('main.a_5') }}</p>
              </div>
            </div>
          </div>

          <div class="col-md-6 col-lg-4 col-xs-12">
            <div class="services-item wow fadeInRight" data-wow-delay="1.8s">
              <div class="icon">
                <i class="lni lni-reload"></i>
              </div>
              <div class="services-content">
                <h3><a class="bold">{{ __('main.q_6') }}</a></h3>
                <p>{{ __('main.a_6') }}</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <div id="feature">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-6 col-md-12 col-sm-12">
            <div class="text-wrapper">
              <div>
                <h2 class="wow fadeInLeft" data-wow-delay="0.3s">{{ __('main.proccess_q') }}</h2>
                <div class="row">
                  <div class="col-md-12 col-sm-12">
                    <div class="features-box wow fadeInLeft" data-wow-delay="0.3s">
                      <div class="features-icon">
                        <i class="lni lni-layers"></i>
                      </div>
                      <div class="features-content">
                        <ul class="process-list">
                        	<li>
                        		1.	{{ __('main.proccess_1') }}
                        	</li>
                        	<li>
                        		2.	{{ __('main.proccess_2') }}
                        	</li>
                        	<li>
                        		3.	{{ __('main.proccess_3') }}
                        	</li>
                        	<li>
                        		4.	{{ __('main.proccess_4') }}
                        	</li>
                        	<li>
                        		5.	{{ __('main.proccess_5') }}
                        	</li>
                        </ul>
                      </div>
                    </div>
                  </div>

                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-6 col-md-12 col-sm-12 padding-none">
            <div class="feature-thumb wow fadeInRight" data-wow-delay="0.3s">
              <img src="assets/img/feature/img-1.jpg" alt="">
            </div>
          </div>
        </div>
      </div>
    </div>

    <section id="services" class="section-padding">
        <div class="container">
            <div class="section-header text-center">

            </div>
            <div class="row">

                <div class="col-md-6 col-lg-4 col-xs-12">
                    <div class="services-item wow fadeInRight" data-wow-delay="0.3s">
                        <div class="icon">
                            <i class="lni lni-alarm-clock"></i>
                        </div>
                        <div class="services-content">
                            <h3><a class="bold">{{ __('main.bot_q1') }}</a></h3>
                            <p>	{{ __('main.bot_a1') }}</p>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-lg-4 col-xs-12">
                    <div class="services-item wow fadeInRight" data-wow-delay="0.6s">
                        <div class="icon">
                            <i class="lni lni-list"></i>
                        </div>
                        <div class="services-content">
                            <h3><a class="bold">{{ __('main.bot_q2') }}</a></h3>
                            <p>
                                <ul>
                                    <li>
                                        - {{ __('main.bot_a2_p1') }}
                                    </li>
                                    <li>
                                        - {{ __('main.bot_a2_p2') }}
                                    </li>
                                    <li>
                                        - {{ __('main.bot_a2_p3') }}
                                    </li>
                                    <li>
                                        - {{ __('main.bot_a2_p4') }}
                                    </li>
                                    <li>
                                        - {{ __('main.bot_a2_p5') }}
                                    </li>
                                </ul>
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-lg-4 col-xs-12">
                    <div class="services-item wow fadeInRight" data-wow-delay="0.9s">
                        <div class="icon">
                            <i class="lni lni-rocket"></i>
                        </div>
                        <div class="services-content">
                            <h3><a class="bold">{{ __('main.bot_q3') }}</a></h3>
                            <p>{{ __('main.bot_a3') }}</p>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-lg-4 col-xs-12">
                    <div class="services-item wow fadeInRight" data-wow-delay="1.2s">
                        <div class="icon">
                            <i class="lni lni-invention"></i>
                        </div>
                        <div class="services-content">
                            <h3><a class="bold">{{ __('main.bot_q4') }}</a></h3>
                            <p>
                                <ul>
                                    <li>
                                        - {{ __('main.bot_a4_p1') }}
                                    </li>
                                    <li>
                                        - {{ __('main.bot_a4_p2') }}
                                    </li>
                                    <li>
                                        - {{ __('main.bot_a4_p3') }}
                                    </li>
                                    <li>
                                        - {{ __('main.bot_a4_p4') }}
                                    </li>
                                    <li>
                                        - {{ __('main.bot_a4_p5') }}
                                    </li>

                                    <li>
                                        - {{ __('main.bot_a4_p6') }}
                                    </li>

                                    <li>
                                        - {{ __('main.bot_a4_p7') }}
                                    </li>

                                    <li>
                                        - {{ __('main.bot_a4_p8') }}
                                    </li>
                                </ul>
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-lg-4 col-xs-12">
                    <div class="services-item wow fadeInRight" data-wow-delay="1.5s">
                        <div class="icon">
                            <i class="lni lni-grow"></i>
                        </div>
                        <div class="services-content">
                            <h3><a class="bold">{{ __('main.bot_q5') }}</a></h3>
                            <p>
                                <ul>
                                    <li>
                                        - {{ __('main.bot_a5_p0') }}
                                    </li>
                                    <li>
                                        - {{ __('main.bot_a5_p1') }}
                                    </li>
                                    <li>
                                        - {{ __('main.bot_a5_p2') }}
                                    </li>

                                    <li>
                                        - {{ __('main.bot_a5_p3') }}
                                    </li>
                                    <li>
                                        - {{ __('main.bot_a5_p4') }}
                                    </li>
                                    <li>
                                        - {{ __('main.bot_a5_p5') }}
                                    </li>
                                </ul>
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-lg-4 col-xs-12">
                    <div class="services-item wow fadeInRight" data-wow-delay="1.8s">
                        <div class="icon">
                            <i class="lni lni-flag"></i>
                        </div>
                        <div class="services-content">
                            <h3><a class="bold">{{ __('main.bot_q6') }}</a></h3>
                            <p>
                            <ul>
                                <li>
                                    - {{ __('main.bot_a6_p1') }}
                                </li>
                                <li>
                                    - {{ __('main.bot_a6_p2') }}
                                </li>

                                <li>
                                    - {{ __('main.bot_a6_p3') }}
                                </li>
                                <li>
                                    - {{ __('main.bot_a6_p4') }}
                                </li>
                            </ul>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
	  <!-- Footer Section Start -->
    <footer id="footer" class="footer-area section-padding">
      <div class="container">
        <div class="container">
          <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12 wow fadeInUp" data-wow-delay="0.2s">
              <div class="footer-logo mb-3">
                <img src="assets/img/itgate_logo2.png" alt="" width="100px">
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12 wow fadeInUp" data-wow-delay="0.4s">
              <h3 class="footer-titel">ITgate</h3>
              <ul>
                <li><a href="#">{{ __('main.menu_user') }}</a></li>
                <li><a href="#">{{ __('main.menu_employer') }}</a></li>
              </ul>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12 wow fadeInUp" data-wow-delay="0.6s">
              <h3 class="footer-titel"> </h3>
              <ul>
                <li><a id="login-link" href="{{ route('login') }}">{{ __('main.login') }}</a></li>
                  <li><a id="login-link" href="{{ route('register') }}">{{ __('main.register') }}</a></li>
              </ul>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12 wow fadeInUp" data-wow-delay="0.8s">
              <h3 class="footer-titel">{{ __('main.find_us') }}</h3>
              <div class="social-icon">
  	            <a class="facebook" href="#"><i class="lni lni lni-facebook-filled"></i></a>
  	            <a class="linkedin" href="#"><i class="lni lni lni-linkedin"></i></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>

    <section id="copyright">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
        </div>
      </div>
    </section>

    <a href="#" class="back-to-top">
    	<i class="lni lni-arrow-up"></i>
    </a>

    <div id="preloader">
      <div class="loader" id="loader-1"></div>
    </div>

    <script src="assets/js/jquery-min.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/owl.carousel.min.js"></script>
    <script src="assets/js/slick.min.js"></script>
    <script src="assets/js/wow.js"></script>
    <script src="assets/js/jquery.nav.js"></script>
    <script src="assets/js/scrolling-nav.js"></script>
    <script src="assets/js/jquery.easing.min.js"></script>
    <script src="assets/js/jquery.slicknav.js"></script>
    <script src="assets/js/main.js"></script>
    <script>
          $(document).on('click', '.pl-lang', function() {
              window.location.href = '/';
          });

          $(document).on('click', '.ru-lang', function() {
              window.location.href = '/ru';
          });

          $(document).on('click', '.ukr-lang', function() {
              window.location.href = '/ukr';
          });
    </script>
    <style>
      .hidden {
        display: none;
      }
      .login-link {
          margin-left: 6px;
      }
    </style>
  </body>
</html>
