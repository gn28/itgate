<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use App\Models\Profile\Town;
use App\Models\Profile\Role;
use App\Models\Profile\Status;
use App\Models\Profile\Currency;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'surname',
        'email',
        'password',
        'status_id',
        'role',
        'phone',
        'city',
        'about',
        'education',
        'photo',
        'cv',
        'git_profile',
        'linked',
        'sallary',
        'currency_id',
        'exp',
        'technologies'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function towns()
    {
        return $this->belongsToMany(Town::class, 'town_user', 'user_id', 'town_id');
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'skill_user', 'user_id', 'skill_id');
    }

    public function status()
    {
        return $this->hasOne(Status::class, 'id', 'status_id');
    }

    public function currency()
    {
        return $this->hasOne(Currency::class, 'id', 'currency_id');
    }
}
