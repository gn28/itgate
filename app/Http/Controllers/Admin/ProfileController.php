<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Profile\Currency;
use App\Models\Profile\Experience;
use App\Models\Profile\Role;
use App\Models\Profile\Status;
use App\Models\Profile\Town;
use App\Models\Profile\Education;
use App\Http\Requests\SaveProfileRequest;
use App\Models\User;
use Illuminate\Support\Facades\Auth;


class ProfileController extends AdminController
{

    public function index(string $locale = NULL) 
    {
        $currencies = Currency::all();
        $experience = Experience::all();
        $roles = Role::all();
        $statuses = Status::all();
        $towns = Town::all();
        $lang = 'pl';
        if (isset($locale)) {
            app()->setLocale($locale);
            if ($locale != 'pl') {
                $lang = $locale;
            }
        }
        
        $user = Auth::user();

        $roleArr = $user->roles()->pluck('profile_roles.id')->toArray();
        $townArr = $user->towns()->pluck('profile_towns.id')->toArray();

        return view('admin.profile.index', compact(
                        'currencies',
                        'experience',
                        'roles',
                        'statuses',
                        'towns',
                        'roleArr',
                        'townArr',
                        'lang',
                        'user'
                    ));
    }


    public function save(SaveProfileRequest $request) 
    {
        $data = $request->getSanitized();

        $user = Auth::user();
        $user->update($data);

        if (isset($data['towns'])) {
            $user->towns()->sync($data['towns']);
        }
        if (isset($data['roles'])) {
            $user->roles()->sync($data['roles']);
        }

        return response()->json(['result' => 'success saving']);
    }


    public function getUnivercites(Request $request)
    {
        $suggestList = Education::where('name', 'like', $request->get('term') . '%')
                                    ->get()
                                    ->pluck('name')
                                    ->toJson();
        
        return $suggestList;
    }

    public function uploadCV(Request $request) 
    {
        $path = '';
        if ($request->file) {
            $path = $request->file->store('/storage/uploads/cv');
        }
        
        if ($path) {
            $user = Auth::user();
            $user->update([
                'cv' => $path
            ]);
        }

        return response()->json(['success']);        
    }

    public function uploadPhoto(Request $request)
    {
        $path = '';
        if ($request->file) {
            $path = $request->file->store('/storage/uploads/photo');
        }
        
        if ($path) {
            $user = Auth::user();
            $user->update([
                'photo' => $path
            ]);
        }

        return response()->json(['success']);
    }

}
