<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;

class DashboardController extends Controller
{
    
    public function index()
    {

        $users = User::where('role', 'user')->paginate(10);
        
        return view('admin.dashboard.index', [
            'users' => $users
        ]);
    }

    public function details(int $id)
    {
        $user = User::find($id);

        return view('admin.dashboard.details', [
            'user' => $user
        ]);
    }
}
