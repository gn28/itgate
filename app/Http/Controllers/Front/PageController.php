<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;

class PageController extends FrontController
{
    
    public function main(string $locale = NULL) {

    	if (isset($locale)) {
            app()->setLocale($locale);
        }
        return view('front.page.main', [
        	'locale' => $locale
       	]);
    }

}
