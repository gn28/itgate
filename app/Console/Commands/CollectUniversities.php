<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Profile\Education;
use Illuminate\Support\Facades\DB;

class CollectUniversities extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'univercity:collect';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Making universities database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            $univerColl = json_decode(file_get_contents('http://universities.hipolabs.com/search'));

            if (!count($univerColl)) {
                Education::truncate();
            }

            $data = [];
            $data[] = [
                'name' => 'Other univercity'
            ];
            foreach ($univerColl as $index => $item) {
                $name = $item->name . ' (' . $item->alpha_two_code . ')';
                $data[] = [
                    'name' => $name
                ];
                echo $index . ' ' . $name, "\n";
            }
            Education::insert($data);
            return 0;
        } catch (Exception $e) {
            echo $e->getMessage(), "\n";
        }
    }

}
