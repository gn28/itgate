<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('surname')->nullable();
            $table->string('email')->unique();
            $table->enum('role', ['user', 'admin']);
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('phone')->nullable();
            $table->string('city')->nullable();
            $table->text('about')->nullable();
            $table->text('technologies')->nullable();
            $table->string('education')->nullable();
            $table->string('photo')->nullable();
            $table->string('git_profile')->nullable();
            $table->string('linked')->nullable();
            $table->string('cv')->nullable();
            $table->tinyInteger('status_id')->nullable();
            $table->tinyInteger('experience_id')->nullable();
            $table->string('sallary')->nullable();
            $table->tinyInteger('currency_id')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
