<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProfileCurrencySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach($this->data as $datum) {
            DB::table('profile_currency')->insert($datum);
        }
    }

    private $data = [
        [
            'name' => 'PLN'
        ],
        [
            'name' => 'EUR'
        ],
        [
            'name' => 'USD'
        ],
        [
            'name' => 'GBP'
        ],
        [
            'name' => 'CHF'
        ]
    ];
}
