<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProfileStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach($this->data as $datum) {
            DB::table('profile_statuses')->insert($datum);
        }
    }

    private $data = [
        [
            'name_pl' => 'Aktywnie poszukuje',
            'name_ru' => 'Активный поиск'
        ],
        [
            'name_pl' => 'Pasywny, otwarty na propozycje',
            'name_ru' => 'Пассивный, открыт для предложений'
        ],
        [
            'name_pl' => 'Nie w wyszukiwaniu',
            'name_ru' => 'Не в поиске'
        ]
    ];
}
