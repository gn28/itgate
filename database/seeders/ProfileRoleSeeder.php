<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProfileRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach($this->data as $datum) {
            DB::table('profile_roles')->insert($datum);
        }

    }

    private $data = [
            [
                'name' => 'Backend Developer',
            ],
            [
                'name' => 'Frontend Developer',
            ],
            [
                'name' => 'Full Stack Developer',
            ],
            [
                'name' => 'Mobile Developer',
            ],
            [
                'name' => 'Embedded Developer',
            ],
            [
                'name' => 'Machine Learning Engineer',
            ],
            [
                'name' => 'Big Data',
            ],
            [
                'name' => 'Data Science',
            ],
            [
                'name' => 'UX Designer',
            ],
            [
                'name' => 'Security Engineer',
            ],
            [
                'name' => 'Network Engineer',
            ],
            [
                'name' => 'Security Engineer',
            ],
            [
                'name' => 'Telco',
            ],
            [
                'name' => 'Network Engineer',
            ],
            [
                'name' => 'Network Administrator',
            ],
            [
                'name' => 'IT Administrator',
            ],
            [
                'name' => 'Database Administrator',
            ],
            [
                'name' => 'Database Developer',
            ],
            [
                'name' => 'DevOps Engineer',
            ],
            [
                'name' => 'IT Architect',
            ],
            [
                'name' => 'Team Leader',
            ],
            [
                'name' => 'Project Manager/Product Owner',
            ],
            [
                'name' => 'SAP',
            ],
            [
                'name' => 'Scrum Master',
            ],
            [
                'name' => 'Blockchain Engineer',
            ],
            [
                'name' => 'ETL Developer',
            ],
            [
                'name' => 'BI',
            ],
            [
                'name' => 'Business Analyst',
            ],
            [
                'name' => 'QA/Testing',
            ],
            [
                'name' => 'Support/Helpdesk',
            ],
            [
                'name' => 'Game Developer',
            ],
            [
                'name' => 'Other',
            ],
    ];
}
