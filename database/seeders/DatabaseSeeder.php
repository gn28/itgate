<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            ProfileExperienceSeeder::class,
            ProfileCurrencySeeder::class,
            ProfileRoleSeeder::class,
            ProfileStatusSeeder::class,
            ProfileTownSeeder::class,
            ProfileEducationSeeder::class,
            UserSeeder::class
        ]);
    }
}
