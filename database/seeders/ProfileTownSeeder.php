<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProfileTownSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach($this->data as $datum) {
            DB::table('profile_towns')->insert($datum);
        }
    }

    private $data = [
            [
                'name' => 'Remote'
            ],
            [
                'name' => 'Warszawa'
            ],
            [
                'name' => 'Białystok'
            ],
            [
                'name' => 'Bielsko-Biała'
            ],
            [
                'name' => 'Bydgoszcz'
            ],
            [
                'name' => 'Bytom'
            ],
            [
                'name' => 'Gdańsk'
            ],
            [
                'name' => 'Gdynia'
            ],
            [
                'name' => 'Gliwice'
            ],
            [
                'name' => 'Katowice'
            ],
            [
                'name' => 'Kielce'
            ],
            [
                'name' => 'Kraków'
            ],
            [
                'name' => 'Lublin'
            ],
            [
                'name' => 'Łódź'
            ],
            [
                'name' => 'Poznań'
            ],
            [
                'name' => 'Rzeszów'
            ],
            [
                'name' => 'Sopot'
            ],
            [
                'name' => 'Szczecin'
            ],
            [
                'name' => 'Toruń'
            ],
            [
                'name' => 'Wrocław'
            ],
            [
                'name' => 'Rybnik'
            ],
            [
                'name' => 'Zielona Góra'
            ]
    ];
}
