<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProfileExperienceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach($this->data as $datum) {
            DB::table('profile_experience')->insert($datum);   
        }
    }

    private $data = [
        [
            'period' => '0-1'
        ],
        [
            'period' => '1-2'
        ],
        [
            'period' => '2-4'
        ],
        [
            'period' => '4-6'
        ],
        [
            'period' => '6-10'
        ]
    ];
}
