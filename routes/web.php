<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Route::group(['namespace' => 'App\Http\Controllers\Admin', 'middleware' => 'auth'], function() {

    Route::get('/profile/user/{locale?}', [
        'uses' => 'ProfileController@index',
        'as' => 'profile.index'
    ])->middleware('can:user');

    Route::post('/profile/user/save', [
        'uses' => 'ProfileController@save',
        'as' => 'profile.save'
    ])->middleware('can:user');

    Route::get('/get/univercities/{locale?}', [
        'uses' => 'ProfileController@getUnivercites',
        'as' => 'profile.get.education'
    ])->middleware('can:user');

    Route::post('/profile/upload/cv', [
        'uses' => 'ProfileController@uploadCV',
        'as' => 'profile.upload.cv'
    ])->middleware('can:user');

    Route::post('/profile/upload/photo', [
        'uses' => 'ProfileController@uploadPhoto',
        'as' => 'profile.upload.photo'
    ])->middleware('can:user');

    Route::get('/dashboard/users', [
        'uses' => 'DashboardController@index',
        'as' => 'dashboard.users.list'
    ])->middleware('can:admin');

    Route::get('/dashboard/user/{id}', [
        'uses' => 'DashboardController@details',
        'as' => 'dashboard.user.details'
    ])->middleware('can:admin');

});

Route::group(['namespace' => 'App\Http\Controllers\Front'], function() {
    
    Route::get('/{locale?}', [
        'uses' => 'PageController@main',
        'as' => 'page.main'
    ]);

});

require __DIR__.'/auth.php';